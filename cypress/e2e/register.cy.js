describe('RegisterForm', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/register');
    });

    it('should successfully register a new user', () => {
        cy.get('#firstname').type('John');
        cy.get('#lastname').type('Doe');
        cy.get('#email').type('john.doe@example.com');
        cy.get('#password').type('password123');
        cy.get('form').submit();

        // Assuming successful registration redirects to '/homescreen'
        cy.url().should('include', '/homescreen');
    });

    it('should display all form fields and buttons', () => {
        cy.get('#firstname').should('exist');
        cy.get('#lastname').should('exist');
        cy.get('#email').should('exist');
        cy.get('#password').should('exist');
        cy.get('button[type="submit"]').should('exist');
        cy.contains('Already have an account? Login').should('exist');
    });

    it('should navigate to login page when "Login" button is clicked', () => {
        cy.contains('Already have an account? Login').click();
        cy.url().should('include', '/');
    });
})