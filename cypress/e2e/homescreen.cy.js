describe('HomeScreen Component', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/homescreen');
    });

    it('should fetch and display breeds', () => {
        cy.intercept('GET', 'http://localhost:8081/api/breeds?page=0&size=4&filter=', { fixture: 'breeds.json' }).as('getBreeds');
        cy.wait('@getBreeds');
        cy.get('.MuiCard-root').should('have.length.greaterThan', 0);
    });

    it('should paginate breeds', () => {
        cy.intercept('GET', 'http://localhost:8081/api/breeds?page=0&size=4&filter=', { fixture: 'breeds.json' }).as('getBreeds');
        cy.wait('@getBreeds');
        cy.get('.MuiTablePagination-root').contains('4').click(); // Changed from '5' to '4' based on the rowsPerPage value in the component
    });

    it('should filter breeds by name', () => {
        const searchTerm = 'Labrador';
        cy.intercept('GET', `http://localhost:8081/api/breeds?page=0&size=4&filter=${searchTerm}`, { fixture: 'filteredBreeds.json' }).as('getFilteredBreeds');
        cy.get('input[type="text"]').type(searchTerm);
        cy.wait('@getFilteredBreeds');
        cy.get('.MuiCard-root').should('have.length.greaterThan', 0);
        cy.get('.MuiCard-root').each((card) => {
            cy.wrap(card).should('contain.text', searchTerm);
        });
    });

    it('should navigate to breed details page when "Details" button is clicked', () => {
        cy.intercept('GET', 'http://localhost:8081/api/breeds?page=0&size=4&filter=', { fixture: 'breeds.json' }).as('getBreeds');
        cy.wait('@getBreeds');
        cy.get('.MuiCard-root').first().find('a').click();
        cy.url().should('include', '/breed/');
    });
});
