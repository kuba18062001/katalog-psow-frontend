import React, { useState, useEffect } from 'react';
import { List, ListItem, ListItemText, Collapse, Typography } from '@mui/material';

function CharacteristicsList({ id }) {
    const [characteristics, setCharacteristics] = useState({});
    const [open, setOpen] = useState(false);

    useEffect(() => {
        const fetchCharacteristics = async () => {
            try {
                const response = await fetch(`http://localhost:8081/api/breeds/${id}/characteristics`);
                if (!response.ok) {
                    throw new Error('Failed to fetch characteristics.');
                }
                const data = await response.json();
                setCharacteristics(data);
            } catch (error) {
                console.error('Error fetching characteristics:', error);
            }
        };

        fetchCharacteristics();
    }, [id]);

    const handleToggle = () => {
        setOpen(!open);
    };

    return (
        <div style={{ maxHeight: '200px', overflowY: 'scroll' }}>
        <div>
            <Typography variant="h6" onClick={handleToggle} style={{ cursor: 'pointer' }}>
                Characteristics
            </Typography>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <ListItem>
                        <ListItemText primary="Size" secondary={characteristics.size} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Temperament" secondary={characteristics.temperament} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Intelligence" secondary={characteristics.intelligence} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Energy" secondary={characteristics.energy} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Exercise Needs" secondary={characteristics.exerciseNeeds} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Activity" secondary={characteristics.activity} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Behaviour With Children" secondary={characteristics.behaviourWithChildren} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Behaviour With Other Animals" secondary={characteristics.behaviourWithOtherAnimals} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Lifespan" secondary={characteristics.lifespan} />
                    </ListItem>
                </List>
            </Collapse>
        </div>
        </div>
    );
}

export default CharacteristicsList;