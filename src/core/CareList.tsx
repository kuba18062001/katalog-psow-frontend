import React, { useState, useEffect } from 'react';
import { List, ListItem, ListItemText, Collapse, Typography } from '@mui/material';

function CareList({ id }) {
    const [care, setCare] = useState({});
    const [open, setOpen] = useState(false);

    useEffect(() => {
        const fetchCare = async () => {
            try {
                const response = await fetch(`http://localhost:8081/api/breeds/${id}/care`);
                if (!response.ok) {
                    throw new Error('Failed to fetch care.');
                }
                const data = await response.json();
                setCare(data);
            } catch (error) {
                console.error('Error fetching care:', error);
            }
        };

        fetchCare();
    }, [id]);

    const handleToggle = () => {
        setOpen(!open);
    };

    return (
        <div style={{ maxHeight: '200px', overflowY: 'scroll' }}>
        <div>
            <Typography variant="h6" onClick={handleToggle} style={{ cursor: 'pointer' }}>
                Care
            </Typography>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <ListItem>
                        <ListItemText primary="Coat Grooming" secondary={care.coatGrooming} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Bathing Requirement" secondary={care.bathingRequirement} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Healthcare Orientation" secondary={care.healthcareOrientation} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Physical Activity Level" secondary={care.physicalActivityLevel} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Living Conditions Adaptability" secondary={care.livingConditionsAdaptability} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Healthy Diet" secondary={care.healthyDiet} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Socialization Need" secondary={care.socializationNeed} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Dental Care" secondary={care.dentalCare} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Mental Activity" secondary={care.mentalActivity} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Walking Requirement" secondary={care.walkingRequirement} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Space Need" secondary={care.spaceNeed} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Human Interaction Need" secondary={care.humanInteractionNeed} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Sleep Need" secondary={care.sleepNeed} />
                    </ListItem>
                </List>
            </Collapse>
        </div>
        </div>
    );
}

export default CareList;
