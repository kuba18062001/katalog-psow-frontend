import HealthList from "./HealthList.tsx";
import CharacteristicsList from "./CharacteristicsList.tsx";
import CommentSection from "./CommentSection.tsx";
import {Card, CardContent, Typography} from "@mui/material";
import {introBodyStyle} from "../config/style.tsx";
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import NavBar from "./NavBar.tsx";
import TrainingAdviceList from "./TrainingAdviceList.tsx";
import CareList from "./CareList.tsx";

function BreedDetails() {
    const { id } = useParams(); // Get the id parameter from the URL
    const [breed, setBreed] = useState(null);

    useEffect(() => {
        const fetchBreedDetails = async () => {
            try {
                const response = await fetch(`http://localhost:8081/api/breeds/${id}`);
                if (!response.ok) {
                    throw new Error('Failed to fetch breed details.');
                }
                const data = await response.json();
                console.log(data);
                setBreed(data);
            } catch (error) {
                console.error('Error fetching breed details:', error);
            }
        };

        fetchBreedDetails();
    }, [id]);

    return (
        <div style={introBodyStyle}>
            {breed && <NavBar id={breed.id} />}
            <main className="App">
                <div className="d-flex flex-column justify-content-evenly w-100">
                    <div className="">
                        <div>
                            {breed ? (
                                <Card>
                                    <CardContent>
                                        <Typography variant="h5" component="h2">
                                            {breed.name}
                                        </Typography>
                                        <Typography color="textSecondary">
                                            ID: {breed.id}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            ) : (
                                <Typography variant="body1" component="p">
                                    Loading...
                                </Typography>
                            )}
                        </div>
                        <div>
                            {/* Check if breed exists before passing its id */}
                            {breed && <CommentSection id={breed.id} />}
                        </div>
                    </div>
                    <div>
                        <div>
                            {/* Check if breed exists before passing its id */}
                            {breed && <HealthList id={breed.id} />}
                        </div>
                        <div>
                            {/* Check if breed exists before passing its id */}
                            {breed && <CharacteristicsList id={breed.id} />}
                        </div>
                        
                        <div>
                            {/* Check if breed exists before passing its id */}
                            {breed && <TrainingAdviceList id={breed.id} />}
                        </div>
                        <div>
                            {/* Check if breed exists before passing its id */}
                            {breed && <CareList id={breed.id} />}
                        </div>
                    </div>
                </div>
            </main>
        </div>
    );
}

export default BreedDetails;