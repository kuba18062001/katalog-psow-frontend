import React, { useState, useEffect } from 'react';
import { List, ListItem, ListItemText, Collapse, Typography } from '@mui/material';

function HealthList({ id }) {
    const [healthData, setHealthData] = useState({});
    const [open, setOpen] = useState(false);

    useEffect(() => {
        const fetchHealthData = async () => {
            try {
                const response = await fetch(`http://localhost:8081/api/breeds/${id}/health`);
                if (!response.ok) {
                    throw new Error('Failed to fetch health data.');
                }
                const data = await response.json();
                setHealthData(data);
            } catch (error) {
                console.error('Error fetching health data:', error);
            }
        };

        fetchHealthData();
    }, [id]);

    const handleToggle = () => {
        setOpen(!open);
    };

    return (
        <div style={{ maxHeight: '200px', overflowY: 'scroll' }}>
        <div>
            <Typography variant="h6" onClick={handleToggle} style={{ cursor: 'pointer' }}>
                Health Data
            </Typography>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <ListItem>
                        <ListItemText primary="Heart Problems" secondary={healthData.heartProblems} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Eye Diseases" secondary={healthData.eyeDiseases} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Skin Problems" secondary={healthData.skinProblems} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Breathing Problems" secondary={healthData.breathingProblems} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Genetic Diseases" secondary={healthData.geneticDiseases} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Pancreatic Problems" secondary={healthData.pancreaticProblems} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Liver Problems" secondary={healthData.liverProblems} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Thyroid Disorders" secondary={healthData.thyroidDisorders} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Epilepsy" secondary={healthData.epilepsy} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Autoimmune Diseases" secondary={healthData.autoimmuneDiseases} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Tumors" secondary={healthData.tumors} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Atopic Dermatitis" secondary={healthData.atopicDermatitis} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Kidney Diseases" secondary={healthData.kidneyDiseases} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Traumatic Musculoskeletal Problems" secondary={healthData.traumaticMusculoskeletalProblems} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Neurological Diseases" secondary={healthData.neurologicalDiseases} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Obesity Tendency" secondary={healthData.obesityTendency} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Digestive System Disorders" secondary={healthData.digestiveSystemDisorders} />
                    </ListItem>
                </List>
            </Collapse>
        </div>
        </div>
    );
}

export default HealthList;
