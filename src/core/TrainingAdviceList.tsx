import React, { useState, useEffect } from 'react';
import { List, ListItem, ListItemText, Collapse, Typography } from '@mui/material';

function TrainingAdviceList({ id }) {
    const [trainingadvices, setTrainingAdvices] = useState({});
    const [open, setOpen] = useState(false);

    useEffect(() => {
        const fetchTrainingAdvices = async () => {
            try {
                const response = await fetch(`http://localhost:8081/api/breeds/${id}/training_advice`);
                if (!response.ok) {
                    throw new Error('Failed to fetch training advices.');
                }
                const data = await response.json();
                setTrainingAdvices(data);
            } catch (error) {
                console.error('Error fetching training advices:', error);
            }
        };

        fetchTrainingAdvices();
    }, [id]);

    const handleToggle = () => {
        setOpen(!open);
    };

    return (
        <div style={{ maxHeight: '200px', overflowY: 'scroll' }}>
        <div>
            <Typography variant="h6" onClick={handleToggle} style={{ cursor: 'pointer' }}>
                Training Advices
            </Typography>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <ListItem>
                        <ListItemText primary="Intelligence" secondary={trainingadvices.intelligence} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Training behavior" secondary={trainingadvices.trainingBehavior} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Reward sensitivity" secondary={trainingadvices.rewardSensitivity} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Activity energy level" secondary={trainingadvices.activityEnergyLevel} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Patience" secondary={trainingadvices.patience} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Human work ability" secondary={trainingadvices.humanWorkAbility} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Reaction speed" secondary={trainingadvices.reactionSpeed} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Child friendly behavior" secondary={trainingadvices.childFriendlyBehavior} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary="Age range" secondary={trainingadvices.ageRange} />
                    </ListItem>
                </List>
            </Collapse>
        </div>
        </div>
    );
}

export default TrainingAdviceList;