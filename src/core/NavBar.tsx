// Importowanie niezbędnych bibliotek i komponentów
import React, { useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { AppBar, Toolbar, Typography, IconButton, useTheme, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button } from '@mui/material';
import LogoutIcon from '@mui/icons-material/Logout';
import DashboardIcon from '@mui/icons-material/Dashboard';

function NavBar() {
    const navigate = useNavigate();
    const location = useLocation();
    const theme = useTheme();
    const [isLogoutDialogOpen, setLogoutDialogOpen] = useState(false);

    // Funkcja do wylogowania
    const logout = () => {
        const token = localStorage.getItem('token');
        if (token) {
            localStorage.removeItem('token');
            navigate("/");
        }
    };

    // Otwiera dialog wylogowania
    const handleOpenLogoutDialog = () => {
        setLogoutDialogOpen(true);
    };

    // Zamyka dialog wylogowania
    const handleCloseLogoutDialog = () => {
        setLogoutDialogOpen(false);
    };

    const LogoutDialog = ({ open, handleClose }) => (
        <Dialog open={open} onClose={handleClose}>
            <DialogTitle>{"Potwierdzenie wylogowania"}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Czy na pewno chcesz się wylogować?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Anuluj</Button>
                <Button onClick={logout} autoFocus>
                    Wyloguj
                </Button>
            </DialogActions>
        </Dialog>
    );

    const renderContent = () => {
        if (location.pathname === "/" || location.pathname === "/register") {
            return null;
        } else {
            return (
                <>
                    <AppBar position="fixed" style={{ background: 'rgba(255, 255, 255, 0.8)', backdropFilter: 'blur(8px)', zIndex: theme.zIndex.drawer + 1 }}>
                        <Toolbar>
                            <Typography variant="h6" component="div" sx={{ flexGrow: 1, color: theme.palette.primary.main }}>
                                Katalog psów
                            </Typography>
                            <IconButton onClick={() => navigate("/homescreen")} sx={{ color: theme.palette.primary.main }} title="Dashboard">
                                <DashboardIcon />
                            </IconButton>
                            <IconButton onClick={handleOpenLogoutDialog} sx={{ color: theme.palette.primary.main }} title="Logout">
                                <LogoutIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <Toolbar /> {/* Ten dodatkowy Toolbar zapewnia, że treść pod AppBar nie zostanie zasłonięta */}
                    <LogoutDialog open={isLogoutDialogOpen} handleClose={handleCloseLogoutDialog} />
                </>
            );
        }
    };

    return renderContent();
}

export default NavBar;
