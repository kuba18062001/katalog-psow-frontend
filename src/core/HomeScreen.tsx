import React, { useState, useEffect } from "react";
import { introBodyStyle } from "../config/style.tsx";
import { Card, CardActions, CardContent, Typography, Grid, Button, Paper, TablePagination, TextField, Box } from '@mui/material';
import { Breed } from "../data/Breed";
import { Link } from "react-router-dom";
import NavBar from "./NavBar.tsx";

function HomeScreen() {
    const [breeds, setBreeds] = useState<Breed[]>([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(4);
    const [filter, setFilter] = useState("");

    useEffect(() => {
        const fetchBreeds = async () => {
            try {
                const response = await fetch(`http://localhost:8081/api/breeds?page=${page}&size=${rowsPerPage}&filter=${filter}`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': 'http://localhost:3000',
                    },
                    credentials: 'include',
                });
                if (!response.ok) {
                    throw new Error('Failed to fetch breeds.');
                }

                const data = await response.json();
                setBreeds(data);
            } catch (error) {
                console.error('Error fetching breeds:', error);
            }
        };

        fetchBreeds();
    }, [page, rowsPerPage, filter]);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleFilterChange = (event) => {
        setFilter(event.target.value);
    };

    const filteredBreeds = breeds.filter((breed) =>
        breed.name.toLowerCase().includes(filter.toLowerCase())
    );

    return (
        <div style={introBodyStyle}>
            <NavBar/>
            <main className="App" style={{ padding: '20px 20px 80px 20px', minHeight: '90vh', overflowX: 'hidden', overflowY: 'auto', width: 'calc(100% - 40px)' }}>
                <Box mb={2}>
                    <TextField
                        label="Filter by name"
                        variant="outlined"
                        value={filter}
                        onChange={handleFilterChange}
                        fullWidth
                    />
                </Box>
                <Grid container spacing={2} justifyContent="center">
                    {filteredBreeds.map((breed, index) => (
                        <Grid item xs={12} sm={6} md={4} lg={3} key={breed.id}>
                            <Card sx={{ minWidth: 275, maxWidth: 345, minHeight: 50, margin: 2, borderRadius: 2, boxShadow: 3, '&:hover': { boxShadow: 8 } }}>
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        {breed.name}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small" color="primary">
                                        <Link to={`/breed/${breed.id}`} style={{ textDecoration: 'none' }}>Details</Link>
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
                <TablePagination
                    rowsPerPageOptions={[4, 8, 12]}
                    component={Paper}
                    count={20} // This should dynamically fetch the total count
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    sx={{ borderTop: '1px solid rgba(224, 224, 224, 1)', marginTop: 2 }}
                />
            </main>
        </div>
    );
}

export default HomeScreen;
