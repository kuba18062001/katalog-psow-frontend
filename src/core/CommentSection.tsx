import React, { useState, useEffect } from 'react';
import { List, ListItem, ListItemText, Typography } from '@mui/material';

function CommentSection({ id }) {
    const [comments, setComments] = useState([]);

    useEffect(() => {
        const fetchComments = async () => {
            try {
                const response = await fetch(`http://localhost:8081/api/breeds/${id}/comments`);
                if (!response.ok) {
                    throw new Error('Failed to fetch comments.');
                }
                const data = await response.json();
                setComments(data);
            } catch (error) {
                console.error('Error fetching comments:', error);
            }
        };

        fetchComments();
    }, [id]);

    return (
        <div>
            <Typography variant="h6">Comments</Typography>
            <List>
                {comments.map(comment => (
                    <ListItem key={comment.id}>
                        <ListItemText
                            primary={comment.content}
                            secondary={`Author: ${comment.author} - Date: ${comment.date}`}
                        />
                    </ListItem>
                ))}
            </List>
        </div>
    );
}

export default CommentSection;
