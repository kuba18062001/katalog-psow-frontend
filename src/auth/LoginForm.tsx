import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import HOST from "../config/apiConst.tsx";
import { introBodyStyle } from "../config/style.tsx";
import { TextField, Button, Card, CardContent, Typography, Container, Box } from '@mui/material';

import PetsIcon from '@mui/icons-material/Pets'; // Ikona psa

function LoginForm() {
    const navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    useEffect(() => {
        const token = localStorage.getItem('token');

        if (token)
            navigate("/homescreen")
    })

    const handleLogin = async (e) => {
        e.preventDefault();
    
        try {
            const response = await fetch(HOST + '/api/authenticate', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': 'http://localhost:3000',
                },
                body: JSON.stringify({
                    email,
                    password,
                }),
                credentials: 'include',
            });
    
            if (!response.ok) {
                throw new Error('Invalid email or password. Please try again.');
            }
    
            const data = await response.json();
            console.log(data.accessToken);
            const token = data.accessToken;
            localStorage.setItem('token', token);
    
            navigate('/homescreen');
        } catch (error) {
            setError('Invalid email or password. Please try again.');
        }
    };
    

    return (
        <div className="App" style={introBodyStyle}>
            <Container component="main" maxWidth="xs" sx={{ 
                mt: 10, 
                display: 'flex', 
                flexDirection: 'column', 
                alignItems: 'center',
                width: '90%', // Zmniejsz szerokość kontenera
            }}>
                <Card sx={{ 
                    bgcolor: 'rgba(255, 255, 255, 0.7)', // Zwiększona przezroczystość
                    backdropFilter: 'blur(8px)', // Efekt rozmycia tła za kartą
                    boxShadow: 1, // Lżejszy cień
                    borderRadius: 2, // Zaokrąglone rogi
                    maxWidth: 360, // Maksymalna szerokość karty, mniejsza niż domyślnie
                }}>
                    <CardContent>
                        <Box sx={{ 
                            display: 'flex', 
                            flexDirection: 'column', 
                            alignItems: 'center', 
                            py: 2 
                        }}>
                            <PetsIcon color="primary" sx={{ fontSize: 50 }} />
                            <Typography component="h1" variant="h6" sx={{ // Mniejszy tekst
                                mt: 2, 
                                mb: 2, 
                                color: 'text.primary' 
                            }}>
                                Login to Dog Breeds Catalog
                            </Typography>
                            <form noValidate onSubmit={handleLogin} sx={{ width: '100%' }}>
                                <TextField
                                    variant="filled" // Zmieniony styl pól
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                    autoFocus
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    sx={{ mb: 2 }}
                                />
                                <TextField
                                    variant="filled" // Zmieniony styl pól
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    sx={{ mb: 3 }}
                                />
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    sx={{ mb: 2 }}
                                >
                                    Log In
                                </Button>
                                <Link to="/register" style={{ textDecoration: 'none' }}>
                                    <Button variant="text" sx={{ color: 'text.secondary' }}>Don't have an account? Register</Button>
                                </Link>
                            </form>
                        </Box>
                    </CardContent>
                </Card>
            </Container>
        </div>
    );
}

export default LoginForm;
