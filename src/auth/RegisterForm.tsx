import React, {useEffect, useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import HOST from "../config/apiConst.tsx";
import {introBodyStyle} from "../config/style.tsx";

import { Container, Card, CardContent, Typography, TextField, Button, Box } from '@mui/material';
import PetsIcon from '@mui/icons-material/Pets';


function RegisterForm() {

    const navigate = useNavigate();
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {
        const token = localStorage.getItem('token');

        if(token)
            navigate("/homescreen")
    })

    const handleRegister = async (e) => {
        e.preventDefault();

        try {
            const response = await fetch(HOST + '/api/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': 'http://localhost:3000',
                },
                body: JSON.stringify({
                    firstname: firstName,
                    lastname: lastName,
                    email,
                    password
                }),
                credentials: 'include', // Dodanie credentials: 'include' odpowiada 'withCredentials: true' w Axios
            });

            if (!response.ok) {
                throw new Error('Something went wrong. Try again');
            }

            const data = await response.json();
            const token = data.accessToken;
            localStorage.setItem('token', token);

            navigate('/homescreen');
        } catch (error) {
            alert(error.message);
        }
    };


    return (
        <div className="App" style={introBodyStyle}>
            <Container component="main" maxWidth="xs" sx={{
                mt: 10,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                width: '90%',
            }}>
                <Card sx={{
                    bgcolor: 'rgba(255, 255, 255, 0.7)',
                    backdropFilter: 'blur(8px)',
                    boxShadow: 1,
                    borderRadius: 2,
                    maxWidth: 360,
                }}>
                    <CardContent>
                        <Box sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            py: 2
                        }}>
                            <PetsIcon color="primary" sx={{ fontSize: 50 }} />
                            <Typography component="h1" variant="h6" sx={{
                                mt: 2,
                                mb: 2,
                                color: 'text.primary'
                            }}>
                                Register for Dog Breeds Catalog
                            </Typography>
                            <form noValidate onSubmit={handleRegister} sx={{ width: '100%' }}>
                                <TextField
                                    variant="filled"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="firstname"
                                    label="First Name"
                                    name="firstname"
                                    autoComplete="firstname"
                                    autoFocus
                                    value={firstName}
                                    onChange={(e) => setFirstName(e.target.value)}
                                    sx={{ mb: 2 }}
                                />
                                <TextField
                                    variant="filled"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="lastname"
                                    label="Last Name"
                                    name="lastname"
                                    autoComplete="lastname"
                                    value={lastName}
                                    onChange={(e) => setLastName(e.target.value)}
                                    sx={{ mb: 2 }}
                                />
                                <TextField
                                    variant="filled"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    sx={{ mb: 2 }}
                                />
                                <TextField
                                    variant="filled"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="password"
                                    label="Password"
                                    type="password"
                                    name="password"
                                    autoComplete="new-password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    sx={{ mb: 3 }}
                                />
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    sx={{ mb: 2 }}
                                >
                                    Register
                                </Button>
                                <Link to="/" style={{ textDecoration: 'none' }}>
                                    <Button variant="text" sx={{ color: 'text.secondary' }}>Already have an account? Login</Button>
                                </Link>
                            </form>
                        </Box>
                    </CardContent>
                </Card>
            </Container>
        </div>
    );
}

export default RegisterForm;
